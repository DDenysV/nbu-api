import React, { useEffect, useState} from "react";
import request from "../../service/request";

function Card() {

    const [data, setData] = useState(null)

    useEffect(()=>{
        request('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json').then((data)=>{setData(data)})
    },[])

    return(<>
        <table>
            <tbody>
                {Array.isArray(data)? data.map((element, index)=>{
                    return (
                    <tr key = {index*2 + 'q'}>
                        <td>{element.txt}</td>
                        <td>{element.cc}</td>
                        <td>Rate {(element.rate).toFixed(2)}</td>
                    </tr>
                        )
                }): null}
            </tbody>
        </table>
    </>)
}

export default Card